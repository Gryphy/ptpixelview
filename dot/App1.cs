using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Surface;
using Microsoft.Surface.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Silverlight_UI_1;
using System.Windows.Forms.Integration;


namespace dot
{
    public class App1 : Microsoft.Xna.Framework.Game
    {
        private readonly GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        MainPage  slider_test = new MainPage();
        MainPage slider_day = new MainPage();

        Model model_object;
        float aspect_ratio;
        Vector3 model_position = new Vector3(150, 0, 0);
        float model_roation = 0.0f;
        Vector3 camera_position = new Vector3(0.0f, 50.0f, 600.0f);

        Button button_sydney = new Button();
        Button button_melbourne = new Button();
        Button button_kul = new Button();

        CheckBox checkbox_bus = new CheckBox();
        CheckBox checkbox_train = new CheckBox();
        CheckBox checkbox_tram = new CheckBox();
        CheckBox checkbox_ferry = new CheckBox();

        /*
        SpriteFont font1;
        Vector2 position;
        string text;
         */
        /*
        Texture2D image;
        Vector2 position1, position2;
        SpriteFont game_font;
        string distance, negate, min, max, length;
         */

        //static DateTime date_time = DateTime.Now;
        //string date = date_time.ToLongDateString();

        int bus = 1;
        int train = 1;
        int tram = 1;

        int menu_bar_x = 10;
        int menu_bar_x_distance = 100;
        int menu_bar_y=10;

        int control_x = 150;
        //int control_x = graphics.PreferredBackBufferWidth - graphics.PreferredBackBufferWidth + 150;
        int control_x_distance = 100;
        int control_y = 650;
        
        string speed = "Speed: ";
        string day = "Day: ";
        string transport = "Transport: ";

        Vector2 position_speed;
        Vector2 position_day;
        Vector2 position_transport;

        Vector2 position_yellow;
        Texture2D image_yellow;
        Vector2 position;
        Vector2 velocity;
        SpriteFont font;
        Texture2D image;
        //Vector2 position_texture;

        float move_speed = 170;
        bool horizontal = true;
        bool vertical = true;

        private TouchTarget touchTarget;
        private Color backgroundColor = new Color(0, 0,0);
        private bool applicationLoadCompleteSignalled;

        private UserOrientation currentOrientation = UserOrientation.Bottom;
        private Matrix screenTransform = Matrix.Identity;

        
        protected TouchTarget TouchTarget
        {
            get { return touchTarget; }
        }

        public App1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        #region Initialization

        private void SetWindowOnSurface()
        {
            System.Diagnostics.Debug.Assert(Window != null && Window.Handle != IntPtr.Zero,
                "Window initialization must be complete before SetWindowOnSurface is called");
            if (Window == null || Window.Handle == IntPtr.Zero)
                return;

           
            Program.InitializeWindow(Window);
            
            graphics.PreferredBackBufferWidth = Program.WindowSize.Width;
            graphics.PreferredBackBufferHeight = Program.WindowSize.Height;
            graphics.ApplyChanges();
            
            Program.PositionWindow();

            
        }

       
        private void InitializeSurfaceInput()
        {
            System.Diagnostics.Debug.Assert(Window != null && Window.Handle != IntPtr.Zero,
                "Window initialization must be complete before InitializeSurfaceInput is called");
            if (Window == null || Window.Handle == IntPtr.Zero)
                return;
            System.Diagnostics.Debug.Assert(touchTarget == null,
                "Surface input already initialized");
            if (touchTarget != null)
                return;

            
            touchTarget = new TouchTarget(Window.Handle, EventThreadChoice.OnBackgroundThread);
            touchTarget.EnableInput();
        }

        #endregion

        #region Overridden Game Methods

       
        protected override void Initialize()
        {
                  
            IsMouseVisible = true; 
            SetWindowOnSurface();
            InitializeSurfaceInput();

            ElementHost e1 = new ElementHost();
            ElementHost e2 = new ElementHost();
            
            currentOrientation = ApplicationServices.InitialOrientation;

            
            ApplicationServices.WindowInteractive += OnWindowInteractive;
            ApplicationServices.WindowNoninteractive += OnWindowNoninteractive;
            ApplicationServices.WindowUnavailable += OnWindowUnavailable;

            Matrix inverted = Matrix.CreateRotationZ(MathHelper.ToRadians(180)) *
                       Matrix.CreateTranslation(graphics.GraphicsDevice.Viewport.Width,
                                                 graphics.GraphicsDevice.Viewport.Height,
                                                 0);

            if (currentOrientation == UserOrientation.Top)
            {
                screenTransform = inverted;
            }
            /*
            position = new Vector2(0, 0);
            text = "this is coding made easy";
            */
            /*
            position1 = new Vector2(50, 100);
            position2 = new Vector2(0, 200);
             */

            //position_texture = new Vector2(0, 0);
            position_speed = new Vector2(10, 600);
            position_day = new Vector2(10, 700);
            position_transport = new Vector2(10, 650);
            position = new Vector2(500, 50);
            position_yellow = new Vector2(700, 100);


            base.Initialize();
            button_set(button_sydney,menu_bar_x ,menu_bar_y ,"Sydney");
            button_set(button_kul, menu_bar_x + menu_bar_x_distance*2, menu_bar_y, "Kul");

            
            speed_set(slider_test,e1,"element_host_1",control_x, control_y-50,700,25 );
            speed_set(slider_day,e2, "element_host_2", control_x, control_y + 50, 700, 25);
            
        }

        private void speed_set(MainPage p_slider,ElementHost p_e,string name, int x, int y, int width, int height)
        {

            p_e.Location = new System.Drawing.Point(x,y);
            p_e.Name = name;
            p_e.Size = new System.Drawing.Size(width, height);
            p_e.Text = name;
            p_e.Child = p_slider ;
            Control.FromHandle(Window.Handle).Controls.Add(p_e);
        }

        private void button_set(Button button_name,int x, int y,string name)
        {
            button_name.Text = name;
            button_name.Location = new System.Drawing.Point(x, y);
            button_name.FlatStyle = FlatStyle.Popup;
            if(name == "Sydney")
            {
                button_name.Click += new System.EventHandler(button_click_syd);
            }
            else if (name == "Meloburne")
            { 
                button_name.Click += new System.EventHandler(button_click_mel);
            }
            else if (name == "Kul")
            {
                button_name.Click += new System.EventHandler(button_click_kul);
            }

            Control.FromHandle(Window.Handle).Controls.Add(button_name);
        }

        private void checkbox_check_bus(object sender, EventArgs e)
        {
            if (checkbox_bus.CheckState == CheckState.Unchecked)
            {
                bus = 0;
            }
            else if (checkbox_bus.CheckState == CheckState.Checked)
            {
                bus = 1;
            }
        }

        private void checkbox_check_train(object sender, EventArgs e)
        {
            if (checkbox_train.CheckState == CheckState.Unchecked)
            {
                train = 0;
            }
            else if (checkbox_train.CheckState == CheckState.Checked)
            {
                train = 1;
            }
        }

        private void checkbox_check_tram(object sender, EventArgs e)
        {
            if (checkbox_tram.CheckState == CheckState.Unchecked)
            {
                tram = 0;
            }
            else if (checkbox_tram.CheckState == CheckState.Checked)
            {
                tram = 1;
            }
        }

        private void checkbox_check_ferry(object sender, EventArgs e)
        {
            if (checkbox_ferry.CheckState == CheckState.Unchecked)
            {
                checkbox_ferry.Text = "No Check";
            }
            else if (checkbox_ferry.CheckState == CheckState.Checked)
            {
                checkbox_ferry.Text = "Yes Check";
            }
        }

        private void set_checkbox(CheckBox checkbox_name,string name, int x, int y)
        {
            checkbox_name.CheckState = CheckState.Checked;
            checkbox_name.Location = new System.Drawing.Point(x, y);
            checkbox_name.Text = name;
            
            if (name == "Bus")
            {
                checkbox_name.BackColor = System.Drawing.Color.Red;
                checkbox_name.CheckStateChanged += new EventHandler(checkbox_check_bus);
            }
            else if (name == "Train")
            {
                checkbox_name.BackColor = System.Drawing.Color.Yellow ;
                checkbox_name.CheckStateChanged += new EventHandler(checkbox_check_train);
            }
            else if (name == "Tram")
            {
                checkbox_name.BackColor = System.Drawing.Color.Blue ;
                checkbox_name.CheckStateChanged += new EventHandler(checkbox_check_tram);
            }
            else if (name == "Ferry")
            {
                checkbox_name.BackColor = System.Drawing.Color.Brown ;
                checkbox_name.CheckStateChanged += new EventHandler(checkbox_check_ferry);    
            }
            
            Control.FromHandle(Window.Handle).Controls.Add(checkbox_name); 
        }

        private void button_click_syd(object sender, EventArgs e)
        {
            //when test on pixelsense
            //checkbox_transport.Location = new System.Drawing.Point(50, 1100);
            
            set_checkbox(checkbox_bus,"Bus",control_x ,control_y );
            set_checkbox(checkbox_train, "Train", control_x + control_x_distance , control_y);
            set_checkbox(checkbox_tram, "Tram", control_x + control_x_distance * 2, control_y);
            set_checkbox(checkbox_ferry, "Ferry", control_x + control_x_distance * 3, control_y);
        }

        private void button_click_mel(object sender, EventArgs e)
        {
            
            set_checkbox(checkbox_bus, "Bus", control_x, control_y);
            set_checkbox(checkbox_train, "Train", control_x + control_x_distance, control_y);
            set_checkbox(checkbox_tram, "Tram", control_x + control_x_distance * 2, control_y);
            checkbox_ferry.Location = new System.Drawing.Point(-50, -50);
        }

        private void button_click_kul(object sender, EventArgs e)
        {
            set_checkbox(checkbox_bus, "Bus", control_x, control_y);
            set_checkbox(checkbox_train, "Train", control_x + control_x_distance, control_y);
            checkbox_tram.Location = new System.Drawing.Point(-50, -50);
            checkbox_ferry.Location = new System.Drawing.Point(-50, -50);
        }

        
        protected override void LoadContent()
        {
            
            spriteBatch = new SpriteBatch(GraphicsDevice);
            /*
            font1 = Content.Load<SpriteFont>("Font/SpriteFont1");
            */
            /*
            image = Content.Load<Texture2D>("Image/sprite1");
            game_font = Content.Load<SpriteFont>("Font/SpriteFont1");
             */

            font = Content.Load<SpriteFont>("Font/SpriteFont1");
            image = Content.Load<Texture2D>("Image/sprite1");
            image_yellow = Content.Load<Texture2D>("Image/yellow");
            aspect_ratio = graphics.GraphicsDevice.Viewport.AspectRatio;

            
        }

        
        protected override void UnloadContent()
        {
            


        }

        
        protected override void Update(GameTime gameTime)
        {
            if (ApplicationServices.WindowAvailability != WindowAvailability.Unavailable)
            {
                if (ApplicationServices.WindowAvailability == WindowAvailability.Interactive)
                {
                    
                }

                
                /*
                distance = Vector2.Distance(position1, position2).ToString();
                negate = Vector2.Negate(position1).ToString();
                min = Vector2.Min(position1, position2).ToString();
                max = Vector2.Max(position1, position2).ToString();
                length = position1.Length().ToString();
                position1 = Vector2.Min(position1, position2);
                 */

                if (horizontal == true)
                {
                    velocity.X = move_speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                }
                else
                {
                    velocity.X = -move_speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                }

                if (vertical == true)
                {
                    velocity.Y = move_speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                }
                else
                {
                    velocity.Y = -move_speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                }


                position.X += velocity.X;
                position.Y += velocity.Y;
                position_yellow.X -= velocity.X;
                position_yellow.Y -= velocity.Y;
                if (position.X < 0 || position.X > graphics.GraphicsDevice.Viewport.Width)
                {

                    if (velocity.X < 0)
                    {
                        horizontal = true;

                    }
                    else
                    {
                        //velocity.X = -move_speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        horizontal = false;
                        //position_texture = new Vector2(100, 100);
                    }
                }

                if (position.Y < 0 || position.Y > graphics.GraphicsDevice.Viewport.Height - 20)
                {
                    if (velocity.Y < 0)
                    {
                        vertical = true;
                    }
                    else
                    {
                        vertical = false;
                    }
                }



                if (position_yellow.X < 0 || position_yellow.X > graphics.GraphicsDevice.Viewport.Width - 20)
                {

                    if (velocity.X < 0)
                    {
                        horizontal = true;

                    }
                    else
                    {
                        //velocity.X = -move_speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
                        horizontal = false;
                        //position_texture = new Vector2(100, 100);
                    }
                }

                if (position_yellow.Y < 0 || position_yellow.Y > graphics.GraphicsDevice.Viewport.Height - 20)
                {
                    if (velocity.Y < 0)
                    {
                        vertical = true;
                    }
                    else
                    {
                        vertical = false;
                    }
                }
            }
            //distance = Vector2.Distance(position1, position2).ToString();
            base.Update(gameTime);
        }

       
        protected override void Draw(GameTime gameTime)
        {
            if (!applicationLoadCompleteSignalled)
            {
                
                ApplicationServices.SignalApplicationLoadComplete();
                applicationLoadCompleteSignalled = true;
            }

            

            GraphicsDevice.Clear(backgroundColor);
            /*
            Matrix[] transforms = new Matrix[model_object.Bones.Count];
            model_object.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model_object.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.PreferPerPixelLighting = true;
                    effect.World = transforms[mesh.ParentBone.Index] * Matrix.CreateRotationY(model_roation)
                        * Matrix.CreateTranslation(model_position);
                    effect.View = Matrix.CreateLookAt(camera_position, Vector3.Zero, Vector3.Up);
                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45.0f), aspect_ratio, 1.0f, 10000.0f);
                }
                mesh.Draw();
            }
             */




            spriteBatch.Begin();
            /*
            spriteBatch.DrawString(font1, text, position, Color.White);
            */
            /*
            spriteBatch.Draw(image, position1, Color.White);
            spriteBatch.Draw(image, position2, Color.White);
            spriteBatch.DrawString(game_font, distance, new Vector2(0, 50), Color.BlueViolet);
            spriteBatch.DrawString(game_font, negate, new Vector2(0, 100), Color.BlueViolet);
            spriteBatch.DrawString(game_font, min, new Vector2(0, 150), Color.BlueViolet);
            spriteBatch.DrawString(game_font, max, new Vector2(0, 2000), Color.BlueViolet);
             */
            //spriteBatch.DrawString(font, date, position_texture, Color.White);
            spriteBatch.DrawString(font, speed, position_speed, Color.White);
            spriteBatch.DrawString(font, day, position_day, Color.White);
            spriteBatch.DrawString(font, transport, position_transport, Color.White);
            if (bus == 1)
            {
                spriteBatch.Draw(image, position, Color.White); 
            }
            if (train == 1)
            {
                spriteBatch.Draw(image_yellow, position_yellow, Color.White);
            }
            
            
            spriteBatch.End();
            

            base.Draw(gameTime);
        }

        #endregion

        #region Application Event Handlers

        
        private void OnWindowInteractive(object sender, EventArgs e)
        {
            
        }

        
        private void OnWindowNoninteractive(object sender, EventArgs e)
        {
            
        }

        
        private void OnWindowUnavailable(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                
                IDisposable graphicsDispose = graphics as IDisposable;
                if (graphicsDispose != null)
                {
                    graphicsDispose.Dispose();
                }
                if (touchTarget != null)
                {
                    touchTarget.Dispose();
                    touchTarget = null;
                }
            }

            
            base.Dispose(disposing);
        }

        #endregion
    }
}
