﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SQLiteGTFSReader
{
    public class ActiveObject
    {
        protected Thread thread;

        /// <summary>
        /// Costructs a new inactive thread
        /// </summary>
        /// <param name="name">Thread name</param>
        public ActiveObject(string name)
        {
            thread = new Thread(Activity);
            thread.Name = name;
        }

        /// <summary>
        /// Starts the thread
        /// </summary>
        public void Start()                              // IGNORE THIS ENTIRE CLASS FOR NOW
        {
            thread.Start();
        }

        /// <summary>
        /// Stops the thread
        /// </summary>
        public void Stop()
        {
            thread.Interrupt();
        }

        /// <summary>
        /// Active Object's 'work' goes here.
        /// Must be overriden.
        /// </summary>
        protected virtual void Activity()
        {

        }
    }
}
