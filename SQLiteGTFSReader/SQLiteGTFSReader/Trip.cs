﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteGTFSReader
{
    class Trip
    {
        private Drawable _draw;
        public Drawable Draw
        {
            get { return _draw; }
            set { _draw = value; }
        }

        private List<double> _longitudes;
        public List<double> Longitudes
        {
            get { return _longitudes; }
            set { _longitudes = value; }
        }

        private List<double> _latitudes;
        public List<double> Latitudes
        {
            get { return _latitudes; }
            set { _latitudes = value; }
        }

        private List<string> _stopName;
        public List<string> StopName
        {
            get { return _stopName; }
            set { _stopName = value; }
        }

        private int _stop_sequence;
        public int StopSequence
        {
            get { return _stop_sequence; }
            set { _stop_sequence = value; }
        }

        private string _tripID;
        public string TripID
        {
            get { return _tripID; }
            set { _tripID = value; }
        }

        private List<DateTime> _stop_times;
        public List<DateTime >stop_times
        {
            get { return _stop_times; }
            set { _stop_times = value; }
        }
        

        public void NextStop()
        {
            _stop_sequence++;
            _draw.X = LatitudeLongitudeParser.getLongitudePercentage(_longitudes[_stop_sequence]);
            _draw.Y = LatitudeLongitudeParser.getLatitudePercentage(_latitudes[_stop_sequence]);
            _draw.Time = _stop_times[_stop_sequence];
        }

        public Trip(List<string> stopName, List<DateTime> stopTimes, string tripID, List<Double> Longs, List<Double> Lats)
        {
            this._stopName = stopName;
            this._tripID = tripID;
            this._stop_sequence = 0;
            this._stop_times = stopTimes;
            this._longitudes = Longs;
            this._latitudes = Lats;

            _draw = new Drawable(LatitudeLongitudeParser.getLongitudePercentage(_longitudes[_stop_sequence]), 
                LatitudeLongitudeParser.getLatitudePercentage(_latitudes[_stop_sequence]), tripID, stopTimes[0]);
        }
    }
}
