﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteGTFSReader
{
    class GTFSDataReader
    {
        private SQLiteConnection connection;
        private SQLiteDataReader reader;
        private SQLiteCommand command;

        /// <summary>
        /// Creates a new reader from a chosen database
        /// </summary>
        /// <param name="DatabaseName">Filename of the databse to load (no extension)</param>
        public GTFSDataReader(string DatabaseName)
        {
            connection = new SQLiteConnection("Data Source=" + DatabaseName + ".sqlite;Version=3;");
            connection.Open();

            string SQL_String = "select * from gtfs";
            command = new SQLiteCommand(SQL_String, connection);
            reader = command.ExecuteReader();
        }

        /// <summary>
        /// Creates a new reader with a fresh iterator
        /// </summary>
        private void ExecuteQuery(string SQL_String)
        {
            command = new SQLiteCommand(SQL_String, connection);
            reader = command.ExecuteReader();
        }

        /// <summary>
        /// Locates any vehicles departing at a certain time and
        /// adds them to the list of current trips
        /// </summary>
        /// <param name="time">Current time</param>
        /// <param name="currentTrips">Existing trips</param>
        /// <returns>Existing trips + new trips</returns>
        public List<Trip> GetNewTrips(string time, List<Trip> currentTrips)
        {
            // Locate any trips at current time
            ExecuteQuery("select * from gtfs WHERE stop_time = '" + time + "' AND stop_sequence = 0");
            List<string> newTrips = new List<string>(); 
            while (reader.Read())
            {
                newTrips.Add(reader["trip_id"].ToString());
            }

            // Remove duplicate trips
            foreach (Trip IDs in currentTrips)
            {
                for (int i = 0; i < newTrips.Count(); i++)
                {
                    if (IDs.TripID == newTrips[i])
                    {
                        newTrips.RemoveAt(i);
                    }
                }
            }

            // Create trip data and append to list
            foreach (string s in newTrips)
            {
                ExecuteQuery("select * from gtfs WHERE trip_id = '" + s + "'");

                List<string> StopNames = new List<string>();
                List<DateTime> StopTimes = new List<DateTime>();
                List<Double> Longitudes = new List<Double>();
                List<Double> Latitudes = new List<Double>();
                int i = 0;
                while (reader.Read())
                {
                    StopNames.Add(reader["stop_name"].ToString());
                    Longitudes.Add(Convert.ToDouble(reader["longitude"]));
                    Latitudes.Add(Convert.ToDouble(reader["latitude"]));

                    DateTime DT = new DateTime();
                    char[] temp = reader["stop_time"].ToString().ToCharArray();
                    if (temp[0] == '2' && int.Parse(temp[1].ToString()) > 3 || temp[0] == '3')
                    {
                        temp[0] = Convert.ToChar((Convert.ToInt32(temp[0]) - 2));
                        temp[1] = Convert.ToChar((Convert.ToInt32(temp[1]) - 4));
                        string timeConv = new string(temp);
                        DT = DateTime.ParseExact(timeConv, "HH:mm:ss", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        string timeConv = new string(temp);
                        DT = DateTime.ParseExact(timeConv, "HH:mm:ss", CultureInfo.InvariantCulture);
                    }

                    StopTimes.Add(DT);
                    i++;
                }
                Trip newTrip = new Trip(StopNames, StopTimes, s, Longitudes, Latitudes);
                currentTrips.Add(newTrip);
            }
            
            return currentTrips;
        }
    }
}
