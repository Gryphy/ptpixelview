﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteGTFSReader
{
    static class LatitudeLongitudeParser
    {
        private static double maxLat;
        private static double minLat;
        private static double maxLong;
        private static double minLong;

        private static SQLiteConnection connection;
        private static SQLiteDataReader reader;
        private static SQLiteCommand command;

        /// <summary>
        /// Creates a new reader from a chosen database
        /// </summary>
        /// <param name="DatabaseName">Filename of the databse to load (no extension)</param>
        static LatitudeLongitudeParser()
        {
            connection = new SQLiteConnection("Data Source=SydneyGTFS.sqlite;Version=3;");
            connection.Open();

            string SQL_String = "select * from gtfs";
            command = new SQLiteCommand(SQL_String, connection);
            reader = command.ExecuteReader();

            getMaxLatitude();
            getMaxLongitude();
            getMinLatitude();
            getMinLongitude();
        }

        /// <summary>
        /// Creates a new reader with a fresh iterator
        /// </summary>
        static void ExecuteQuery(string SQL_String)
        {
            command = new SQLiteCommand(SQL_String, connection);
            reader = command.ExecuteReader();
        }

        /// <summary>
        /// Calculates where a dot should be placed on screen
        /// by finding the distance between the maximum and minimum
        /// latitudes
        /// </summary>
        /// <param name="TripID">Unique ID for the trip</param>
        /// <param name="stop_sequence">Stop the vehicle is at</param>
        /// <returns>Percentage between max/min latitude</returns>
        public static double getLatitudePercentage(double Latitude)
        {
            return (Latitude - minLat) / (maxLat - minLat) * 100;
        }

        /// <summary>
        /// Calculates where a dot should be placed on screen
        /// by finding the distance between the maximum and minimum
        /// longitudes
        /// </summary>
        /// <param name="TripID">Unique ID for the trip</param>
        /// <param name="stop_sequence">Stop the vehicle is at</param>
        /// <returns>Percentage between max/min longitudes</returns>
        public static double getLongitudePercentage(double Longitude)
        {
            return (Longitude - minLong) / (maxLong - minLong) * 100;
        }

        /// <summary>
        /// Finds the maximum latitude in the database
        /// </summary>
        /// <returns>Maximum latitude</returns>
        static double getMaxLatitude()
        {
            double result = -999999999999999999;
            ExecuteQuery("select * from gtfs");
            while (reader.Read())
            {
                if (Convert.ToDouble(reader["latitude"]) > result)
                {
                    result = Convert.ToDouble(reader["latitude"]);
                }
            }
            maxLat = result;
            return result;
        }

        /// <summary>
        /// Finds the minimum latitude in the database
        /// </summary>
        /// <returns>Minimum latitude</returns>
        static double getMinLatitude()
        {
            double result = 999999999999999999;
            ExecuteQuery("select * from gtfs");
            while (reader.Read())
            {
                if (Convert.ToDouble(reader["latitude"]) < result)
                {
                    result = Convert.ToDouble(reader["latitude"]);
                }
            }
            minLat = result;
            return result;
        }

        /// <summary>
        /// Finds the maximum longitude in the database
        /// </summary>
        /// <returns>Maximum longitude</returns>
        static double getMaxLongitude()
        {
            double result = -999999999999999999;
            ExecuteQuery("select * from gtfs");
            while (reader.Read())
            {
                if (Convert.ToDouble(reader["longitude"]) > result)
                {
                    result = Convert.ToDouble(reader["longitude"]);
                }
            }
            maxLong = result;
            return result;
        }

        /// <summary>
        /// Finds the minumum longitude in the database
        /// </summary>
        /// <returns>Minimum longitude</returns>
        static double getMinLongitude()
        {
            double result = 999999999999999999;
            ExecuteQuery("select * from gtfs");
            while (reader.Read())
            {
                if (Convert.ToDouble(reader["longitude"]) < result)
                {
                    result = Convert.ToDouble(reader["longitude"]);
                }
            }
            minLong = result;
            return result;
        }
    }
}
