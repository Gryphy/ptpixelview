﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SQLiteGTFSReader
{
    class Program
    {
        static void Main(string[] args)
        {
            TripUpdater tripUp = new TripUpdater("08:30:00", "SydneyGTFS");

            int A0 = 0; int A1 = 0; int A2 = 0; int A3 = 0; int A4 = 0; int A5 = 0; int A6 = 0; int A7 = 0; int A8 = 0; int A9 = 0;
            int B0 = 0; int B1 = 0; int B2 = 0; int B3 = 0; int B4 = 0; int B5 = 0; int B6 = 0; int B7 = 0; int B8 = 0; int B9 = 0;
            int C0 = 0; int C1 = 0; int C2 = 0; int C3 = 0; int C4 = 0; int C5 = 0; int C6 = 0; int C7 = 0; int C8 = 0; int C9 = 0;
            int D0 = 0; int D1 = 0; int D2 = 0; int D3 = 0; int D4 = 0; int D5 = 0; int D6 = 0; int D7 = 0; int D8 = 0; int D9 = 0;
            int E0 = 0; int E1 = 0; int E2 = 0; int E3 = 0; int E4 = 0; int E5 = 0; int E6 = 0; int E7 = 0; int E8 = 0; int E9 = 0;
            int F0 = 0; int F1 = 0; int F2 = 0; int F3 = 0; int F4 = 0; int F5 = 0; int F6 = 0; int F7 = 0; int F8 = 0; int F9 = 0;
            int G0 = 0; int G1 = 0; int G2 = 0; int G3 = 0; int G4 = 0; int G5 = 0; int G6 = 0; int G7 = 0; int G8 = 0; int G9 = 0;
            int H0 = 0; int H1 = 0; int H2 = 0; int H3 = 0; int H4 = 0; int H5 = 0; int H6 = 0; int H7 = 0; int H8 = 0; int H9 = 0;
            int I0 = 0; int I1 = 0; int I2 = 0; int I3 = 0; int I4 = 0; int I5 = 0; int I6 = 0; int I7 = 0; int I8 = 0; int I9 = 0;
            int J0 = 0; int J1 = 0; int J2 = 0; int J3 = 0; int J4 = 0; int J5 = 0; int J6 = 0; int J7 = 0; int J8 = 0; int J9 = 0;

            while (true)
            {
                if (DrawableStorage.Instance.ToDraw.Count() != 0)
                {
                    A0 = 0; A1 = 0; A2 = 0; A3 = 0; A4 = 0; A5 = 0; A6 = 0; A7 = 0; A8 = 0; A9 = 0;
                    B0 = 0; B1 = 0; B2 = 0; B3 = 0; B4 = 0; B5 = 0; B6 = 0; B7 = 0; B8 = 0; B9 = 0;
                    C0 = 0; C1 = 0; C2 = 0; C3 = 0; C4 = 0; C5 = 0; C6 = 0; C7 = 0; C8 = 0; C9 = 0;
                    D0 = 0; D1 = 0; D2 = 0; D3 = 0; D4 = 0; D5 = 0; D6 = 0; D7 = 0; D8 = 0; D9 = 0;
                    E0 = 0; E1 = 0; E2 = 0; E3 = 0; E4 = 0; E5 = 0; E6 = 0; E7 = 0; E8 = 0; E9 = 0;
                    F0 = 0; F1 = 0; F2 = 0; F3 = 0; F4 = 0; F5 = 0; F6 = 0; F7 = 0; F8 = 0; F9 = 0;
                    G0 = 0; G1 = 0; G2 = 0; G3 = 0; G4 = 0; G5 = 0; G6 = 0; G7 = 0; G8 = 0; G9 = 0;
                    H0 = 0; H1 = 0; H2 = 0; H3 = 0; H4 = 0; H5 = 0; H6 = 0; H7 = 0; H8 = 0; H9 = 0;
                    I0 = 0; I1 = 0; I2 = 0; I3 = 0; I4 = 0; I5 = 0; I6 = 0; I7 = 0; I8 = 0; I9 = 0;
                    J0 = 0; J1 = 0; J2 = 0; J3 = 0; J4 = 0; J5 = 0; J6 = 0; J7 = 0; J8 = 0; J9 = 0;

                    Thread.Sleep(20);

                    Console.Clear();

                    Console.WriteLine("10 x 10 Map of Sydney Public Transpoort (1 number = 1 vehicle)");

                    foreach (Drawable d in DrawableStorage.Instance.GetDrawableList())
                    {
                        if (d.X < 10 && d.Y < 10) { A0++; }
                        else if (d.X < 20 && d.Y < 10) { A1++; }
                        else if (d.X < 30 && d.Y < 10) { A2++; }
                        else if (d.X < 40 && d.Y < 10) { A3++; }
                        else if (d.X < 50 && d.Y < 10) { A4++; }
                        else if (d.X < 60 && d.Y < 10) { A5++; }
                        else if (d.X < 70 && d.Y < 10) { A6++; }
                        else if (d.X < 80 && d.Y < 10) { A7++; }
                        else if (d.X < 90 && d.Y < 10) { A8++; }
                        else if (d.X < 100 && d.Y < 10) { A9++; }
                        else if (d.X < 10 && d.Y < 20) { B0++; }
                        else if (d.X < 20 && d.Y < 20) { B1++; }
                        else if (d.X < 30 && d.Y < 20) { B2++; }
                        else if (d.X < 40 && d.Y < 20) { B3++; }
                        else if (d.X < 50 && d.Y < 20) { B4++; }
                        else if (d.X < 60 && d.Y < 20) { B5++; }
                        else if (d.X < 70 && d.Y < 20) { B6++; }
                        else if (d.X < 80 && d.Y < 20) { B7++; }
                        else if (d.X < 90 && d.Y < 20) { B8++; }
                        else if (d.X < 100 && d.Y < 20) { B9++; }
                        else if (d.X < 10 && d.Y < 30) { C0++; }
                        else if (d.X < 20 && d.Y < 30) { C1++; }
                        else if (d.X < 30 && d.Y < 30) { C2++; }
                        else if (d.X < 40 && d.Y < 30) { C3++; }
                        else if (d.X < 50 && d.Y < 30) { C4++; }
                        else if (d.X < 60 && d.Y < 30) { C5++; }
                        else if (d.X < 70 && d.Y < 30) { C6++; }
                        else if (d.X < 80 && d.Y < 30) { C7++; }
                        else if (d.X < 90 && d.Y < 30) { C8++; }
                        else if (d.X < 100 && d.Y < 30) { C9++; }
                        else if (d.X < 10 && d.Y < 40) { D0++; }
                        else if (d.X < 20 && d.Y < 40) { D2++; }
                        else if (d.X < 30 && d.Y < 40) { D6++; }
                        else if (d.X < 40 && d.Y < 40) { D3++; }
                        else if (d.X < 50 && d.Y < 40) { D4++; }
                        else if (d.X < 60 && d.Y < 40) { D5++; }
                        else if (d.X < 70 && d.Y < 40) { D6++; }
                        else if (d.X < 80 && d.Y < 40) { D7++; }
                        else if (d.X < 90 && d.Y < 40) { D8++; }
                        else if (d.X < 100 && d.Y < 40) { D9++; }
                        else if (d.X < 10 && d.Y < 50) { E0++; }
                        else if (d.X < 20 && d.Y < 50) { E1++; }
                        else if (d.X < 30 && d.Y < 50) { E2++; }
                        else if (d.X < 40 && d.Y < 50) { E3++; }
                        else if (d.X < 50 && d.Y < 50) { E4++; }
                        else if (d.X < 60 && d.Y < 50) { E5++; }
                        else if (d.X < 70 && d.Y < 50) { E6++; }
                        else if (d.X < 80 && d.Y < 50) { E7++; }
                        else if (d.X < 90 && d.Y < 50) { E8++; }
                        else if (d.X < 100 && d.Y < 50) { E9++; }
                        else if (d.X < 10 && d.Y < 60) { F0++; }
                        else if (d.X < 20 && d.Y < 60) { F1++; }
                        else if (d.X < 30 && d.Y < 60) { F2++; }
                        else if (d.X < 40 && d.Y < 60) { F3++; }
                        else if (d.X < 50 && d.Y < 60) { F4++; }
                        else if (d.X < 60 && d.Y < 60) { F5++; }
                        else if (d.X < 70 && d.Y < 60) { F6++; }
                        else if (d.X < 80 && d.Y < 60) { F7++; }
                        else if (d.X < 90 && d.Y < 60) { F8++; }
                        else if (d.X < 100 && d.Y < 60) { F9++; }
                        else if (d.X < 10 && d.Y < 70) { G0++; }
                        else if (d.X < 20 && d.Y < 70) { G1++; }
                        else if (d.X < 30 && d.Y < 70) { G2++; }
                        else if (d.X < 40 && d.Y < 70) { G3++; }
                        else if (d.X < 50 && d.Y < 70) { G4++; }
                        else if (d.X < 60 && d.Y < 70) { G5++; }
                        else if (d.X < 70 && d.Y < 70) { G6++; }
                        else if (d.X < 80 && d.Y < 70) { G7++; }
                        else if (d.X < 90 && d.Y < 70) { G8++; }
                        else if (d.X < 100 && d.Y < 70) { G9++; }
                        else if (d.X < 10 && d.Y < 80) { H0++; }
                        else if (d.X < 20 && d.Y < 80) { H1++; }
                        else if (d.X < 30 && d.Y < 80) { H2++; }
                        else if (d.X < 40 && d.Y < 80) { H3++; }
                        else if (d.X < 50 && d.Y < 80) { H4++; }
                        else if (d.X < 60 && d.Y < 80) { H5++; }
                        else if (d.X < 70 && d.Y < 80) { H6++; }
                        else if (d.X < 80 && d.Y < 80) { H7++; }
                        else if (d.X < 90 && d.Y < 80) { H8++; }
                        else if (d.X < 100 && d.Y < 80) { H9++; }
                        else if (d.X < 10 && d.Y < 90) { I0++; }
                        else if (d.X < 20 && d.Y < 90) { I1++; }
                        else if (d.X < 30 && d.Y < 90) { I2++; }
                        else if (d.X < 40 && d.Y < 90) { I3++; }
                        else if (d.X < 50 && d.Y < 90) { I4++; }
                        else if (d.X < 60 && d.Y < 90) { I5++; }
                        else if (d.X < 70 && d.Y < 90) { I6++; }
                        else if (d.X < 80 && d.Y < 90) { I7++; }
                        else if (d.X < 90 && d.Y < 90) { I8++; }
                        else if (d.X < 100 && d.Y < 90) { I9++; }
                        else if (d.X < 10 && d.Y < 100) { J0++; }
                        else if (d.X < 20 && d.Y < 100) { J1++; }
                        else if (d.X < 30 && d.Y < 100) { J2++; }
                        else if (d.X < 40 && d.Y < 100) { J3++; }
                        else if (d.X < 50 && d.Y < 100) { J4++; }
                        else if (d.X < 60 && d.Y < 100) { J5++; }
                        else if (d.X < 70 && d.Y < 100) { J6++; }
                        else if (d.X < 80 && d.Y < 100) { J7++; }
                        else if (d.X < 90 && d.Y < 100) { J8++; }
                        else if (d.X < 100 && d.Y < 100) { J9++; }

                    }

                    Console.WriteLine(A0 + "\t" + A1 + "\t" + A6 + "\t" + A3 + "\t" + A4 + "\t" + A5 + "\t" + A6 + "\t" + A7 + "\t" + A8 + "\t" + A9 + "\t" + "\n" +
                        B0 + "\t" + B1 + "\t" + B6 + "\t" + B3 + "\t" + B4 + "\t" + B5 + "\t" + B6 + "\t" + B7 + "\t" + B8 + "\t" + B9 + "\t" + "\n" +
                        C0 + "\t" + C1 + "\t" + C6 + "\t" + C3 + "\t" + C4 + "\t" + C5 + "\t" + C6 + "\t" + C7 + "\t" + C8 + "\t" + C9 + "\t" + "\n" +
                        D0 + "\t" + D1 + "\t" + D6 + "\t" + D3 + "\t" + D4 + "\t" + D5 + "\t" + D6 + "\t" + D7 + "\t" + D8 + "\t" + D9 + "\t" + "\n" +
                        E0 + "\t" + E1 + "\t" + E6 + "\t" + E3 + "\t" + E4 + "\t" + E5 + "\t" + E6 + "\t" + E7 + "\t" + E8 + "\t" + E9 + "\t" + "\n" +
                        F0 + "\t" + F1 + "\t" + F6 + "\t" + F3 + "\t" + F4 + "\t" + F5 + "\t" + F6 + "\t" + F7 + "\t" + F8 + "\t" + F9 + "\t" + "\n" +
                        G0 + "\t" + G1 + "\t" + G6 + "\t" + G3 + "\t" + G4 + "\t" + G5 + "\t" + G6 + "\t" + G7 + "\t" + G8 + "\t" + G9 + "\t" + "\n" +
                        H0 + "\t" + H1 + "\t" + H6 + "\t" + H3 + "\t" + H4 + "\t" + H5 + "\t" + H6 + "\t" + H7 + "\t" + H8 + "\t" + H9 + "\t" + "\n" +
                        I0 + "\t" + I1 + "\t" + I6 + "\t" + I3 + "\t" + I4 + "\t" + I5 + "\t" + I6 + "\t" + I7 + "\t" + I8 + "\t" + I9 + "\t" + "\n" +
                        J0 + "\t" + J1 + "\t" + J6 + "\t" + J3 + "\t" + J4 + "\t" + J5 + "\t" + J6 + "\t" + J7 + "\t" + J8 + "\t" + J9);
                }
            }
        }
    }
}
