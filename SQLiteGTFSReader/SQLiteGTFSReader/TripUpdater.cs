﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Timers;

namespace SQLiteGTFSReader
{
    class TripUpdater
    {            
        private List<Trip> trips;
        private GTFSDataReader dataReader;
        private DateTime DT;
        private Timer t;

        public TripUpdater(string startTime, string databaseName)
        {
            dataReader = new GTFSDataReader(databaseName);

            trips = new List<Trip>();

            DT = new DateTime();
            DT = DateTime.ParseExact(startTime, "HH:mm:ss", CultureInfo.InvariantCulture);

            t = new Timer();
            t.Interval = 1;
            t.AutoReset = true;
            t.Elapsed += new ElapsedEventHandler(TimerElapsed);
            t.Start();
        }

        void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            lock (this)
            {
                trips = (dataReader.GetNewTrips(DT.TimeOfDay.ToString(), trips));

                PushTripsToStorage(trips);

                // for every trip
                for (int j = 0; j < trips.Count(); j++)
                {
                    // if that trip has a stop matching 
                    if (trips[j].StopSequence != trips[j].stop_times.Count() - 1)
                    {
                        if (trips[j].stop_times[trips[j].StopSequence] == DT)
                        {
                            // Move that trip to the next stop
                            trips[j].NextStop();
                        }
                    }
                    else
                    {
                        trips.RemoveAt(j);
                    }
                }

                DT = DT + TimeSpan.Parse("00:00:05");
            }
        }

        private void PushTripsToStorage(List<Trip> trips)
        {
            List<Drawable> drawList = new List<Drawable>();
            for (int i = 0; i < trips.Count(); i++)
            {
                Drawable vehiclePos = trips[i].Draw;
                drawList.Add(vehiclePos);
            }
            if (DrawableStorage.Instance.ToDraw.Count() > 10000)
            {
                System.Threading.Thread.Sleep(500);
            }
            DrawableStorage.Instance.PushSet(drawList);
        }

        public void setUpdateSpeed(int ms)
        {
            t.Interval = ms;
        }
    }
}
