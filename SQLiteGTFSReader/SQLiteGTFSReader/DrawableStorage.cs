﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteGTFSReader
{
    public class DrawableStorage
    {
        private Queue<List<Drawable>> toDraw;
        internal Queue<List<Drawable>> ToDraw
        {
            get { return toDraw; }
        }

        private static DrawableStorage _instance = new DrawableStorage();
        public static DrawableStorage Instance 
        { 
            get { return _instance; } 
        }
        
        private DrawableStorage() 
        {
            toDraw = new Queue<List<Drawable>>();
        }

        static DrawableStorage() 
        {
            
        }

        internal List<Drawable> GetDrawableList()
        { 
            return toDraw.Dequeue();
        }

        internal void PushSet(List<Drawable> drawableList)
        {
            toDraw.Enqueue(drawableList);
        }
    }
}
