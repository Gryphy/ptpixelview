﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteGTFSReader
{
    class Drawable
    {
        private double _X;
        public double X
        {
            get { return _X; }
            set { _X = value; }
        }

        private double _Y;
        public double Y
        {
            get { return _Y; }
            set { _Y = value; }
        }

        private string _ID;
        public string ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        private DateTime _time;
        public DateTime Time
        {
            get { return _time; }
            set { _time = value; }
        }

        public Drawable(double X, double Y, string ID, DateTime time)
        {
            this._X = X;
            this._Y = Y;
            this._ID = ID;
            this._time = time;
        }
    }
}
