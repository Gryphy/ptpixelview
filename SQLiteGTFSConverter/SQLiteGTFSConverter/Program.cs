﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLiteGTFSConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            SQLiteConnection.CreateFile("SydneyGTFS.sqlite");
            
            SQLiteConnection m_dbConnection;

            m_dbConnection =
                new SQLiteConnection("Data Source=SydneyGTFS.sqlite;Version=3;");
            m_dbConnection.Open();

            string sql = "CREATE TABLE gtfs (trip_id VARCHAR(26), stop_time VARCHAR(9), stop_id INT(9), stop_sequence INT(3), stop_name VARCHAR(40), latitude VARCHAR(20), longitude VARCHAR(20))";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

            string line;
            bool ignoretimestitle = false;

            System.IO.StreamReader filetimes =
               new System.IO.StreamReader("stop_times.txt");

            System.IO.StreamReader filestops =
               new System.IO.StreamReader("stops.txt");

            List<List<string>> data = new List<List<string>>(); 
            while ((line = filestops.ReadLine()) != null)
            {
                string[] splitter = line.Split(',');
                List<string> files = new List<string>();
                files.Add(splitter[0]);
                files.Add(splitter[2]);
                files.Add(splitter[3]);
                files.Add(splitter[4]);
                data.Add(files);
            }

            string stopname = "", latitude = "", longitude = "";
            while ((line = filetimes.ReadLine()) != null)
            {
                if (ignoretimestitle == true)
                {
                    string[] splitter = line.Split(',');
                    
                    foreach (List<string> stops in data)
                    {
                        if (splitter[3] == stops[0])
                        {
                            stopname = stops[1];
                            latitude = stops[2];
                            longitude = stops[3];
                        }
                    }
                    sql = "insert into gtfs (trip_id, stop_time, stop_id, stop_sequence, stop_name, latitude, longitude) values ('" + 
                        splitter[0] + "', '" + splitter[2] + "'," + splitter[3] + "," + splitter[4] +", '" + stopname + "', '" + 
                        latitude + "', '" + longitude + "')";
                    
                    command.CommandText = sql;
                    command.ExecuteNonQuery();
                }
                else
                {
                    ignoretimestitle = true;
                }
            }

            filetimes.Close();
            filestops.Close();
        }
    }
}
